import React from "react";
import Cart from "./components/Cart";
import items from "./items.json";
import "./index.css";

interface Image {
  [key: string]: string;
}

interface Option {
  [key: string]: string | undefined;
}

interface Description {
  [key: string]: string | undefined;
}

export interface Item {
  title: string;
  image: Image;
  option: Option;
  description: Description;
}

export interface CartProps {
  data: Item;
}

const importedItems: Item[] = items as Item[];

const App: React.FC = () => {
  return (
    <div>
      <div className="mx-auto m-0 w-container md: max-w-container h-container my-[108px]">
        <h1 className="mx-auto text-center text-header w-header font-header font-bold text-darkBlue leading-header mb-20">
          We make creative media that
          <span className="text-blue"> adds value</span>
        </h1>
        <div className="flex justify-center flex-wrap gap-[30px]">
          {importedItems.map((item, index) => (
            <Cart key={index} data={item} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default App;
