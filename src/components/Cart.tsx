import React, { FC } from "react";
import { Item } from "../App";

interface CartProps {
  data: Item;
}

const Cart: FC<CartProps> = ({ data }) => {
  return (
    <div className="flex w-block rounded-lg shadow-sm missing">
      <div className="flex flex-col gap-0.5">
        {Object.values(data.image).map((img, i) => (
          <img src={img} key={i} alt={img} />
        ))}
      </div>
      <div className="flex flex-col gap-3 w-[254px] bg-white py-10 px-8 text-charcoal font-bold">
        <h2 className="font-header text-2xl">{data.title}</h2>
        {data.option && (
          <ul className="custom-list font-body text-info leading-6">
            {Object.values(data.option).map((item, i) => (
              <li key={i} className="font-body text-info leading-6 font-bold ">
                {item}
              </li>
            ))}
          </ul>
        )}
        {Object.values(data.description).map((desc, i) => (
          <p key={i} className="font-body text-info leading-6 font-semibold ">
            {desc}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Cart