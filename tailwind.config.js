/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    colors: {
      blue: "#506BCA",
      darkBlue: "#1B264F",
      white: "#ffffff",
      charcoal: "#353844",
    },
    fontSize: {
      info: "15px",
      header: "55px",
    },
    fontFamily: {
      header: ["Poppins", "sans-serif"],
      body: ["Nunito Sans", "sans-serif"],
    },
    width: {
      container: "1373px",
      header: "730px",
      block: "437px",
    },
    height: {
      container: "1042px",
    },
    lineHeight: {
      header: "55px",
    },
  },
  plugins: [],
};
